[![Release](https://jitpack.io/v/org.traffxml/traff-lvceli.svg)](https://jitpack.io/#org.traffxml/traff-lvceli)

# Additional resources

milestones.csv is required to resolve kilometric points to coordinates. It is available from https://lvceli.lv/maps/csv/CSV_LVC_KM.csv under a CC-0 license.

# Javadoc

Javadoc for `master` is at https://traffxml.gitlab.io/traff-lvceli/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-lvceli/javadoc/dev/.
