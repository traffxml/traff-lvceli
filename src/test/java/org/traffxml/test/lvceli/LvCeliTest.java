package org.traffxml.test.lvceli;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.traffxml.lvceli.LvCeliFeed;
import org.traffxml.lvceli.LvCeliMessage;
import org.traffxml.lvceli.LvCeliMessage.Point;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.viewer.TraffViewer;
import org.traffxml.viewer.input.ConverterSource;
import org.traffxml.viewer.input.StaticSource;

public class LvCeliTest {
	private static final LvCeliConverter CONVERTER = new LvCeliConverter();
	// FIXME find a way to pass a list of URLs instead of just a single one
	private static final String DEFAULT_URL = "https://lvceli.lv/maps/CSV/CSV_SIC_{BO_A|BO_TP|CLOSED}.csv";
	private static final int FLAG_CONVERT = 0x1;
	private static final int FLAG_DISSECT = 0x2;
	private static final int FLAG_GUI = 0x10000;
	private static final int MASK_OPERATIONS = 0xFFFF;

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -convert <feed>  Convert <feed> to TraFF");
		System.out.println("  -dissect <feed>  Examine <feed> and print results");
		System.out.println("  -gui             Launch TraFF Viewer UI (not with -dissect)");
		System.out.println();
		System.out.println("<feed> can refer to a local file or an http/https URL");
	}

	public static void main(String[] args) {
		LvCeliFeed feed = null;
		String input = null;
		int flags = 0;

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (int i = 0; i < args.length; i++) {
			if ("-convert".equals(args[i])) {
				input = getParam("convert", args, ++i);
				flags |= FLAG_CONVERT;
			} else if ("-dissect".equals(args[i])) {
				input = getParam("dissect", args, ++i);
				flags |= FLAG_DISSECT;
			} else if ("-gui".equals(args[i])) {
				flags |= FLAG_GUI;
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		if (flags == 0) {
			System.err.println("A valid option must be specified.");
			System.exit(1);
		}
		if (flags == FLAG_GUI) {
			TraffViewer.launch(CONVERTER, DEFAULT_URL);
		} else {
			if (input.startsWith("http://") || input.startsWith("https://")) {
				URL url;
				try {
					url = new URL(input);
					feed = CONVERTER.parse(url);
				} catch (MalformedURLException e) {
					System.err.println("Input URL is invalid. Aborting.");
					System.exit(1);
				} catch (IOException e) {
					System.err.println("Cannot read from input URL. Aborting.");
					System.exit(1);
				}
			} else {
				File inputFile = new File(input);
				try {
					feed = CONVERTER.parse(inputFile);
				} catch (FileNotFoundException e) {
					System.err.println("Input file does not exist. Aborting.");
					System.exit(1);
				} catch (IllegalArgumentException e) {
					System.err.println(String.format("%s. Aborting.", e.getMessage()));
					System.exit(1);
				}
			}
			if (feed != null) {
				switch(flags & MASK_OPERATIONS) {
				case FLAG_CONVERT:
					convertFeed(feed, (flags & FLAG_GUI) != 0);
					break;
				case FLAG_DISSECT:
					dissectFeed(feed);
					break;
				default:
					System.err.println("Invalid combination of options. Aborting.");
					System.exit(1);
				}
			} else {
				System.err.println("Feed is null. Aborting.");
				System.exit(1);
			}
		}
	}

	static void convertFeed(LvCeliFeed feed, boolean gui) {
		try {
			TraffFeed tFeed = CONVERTER.convert(feed);
			if (gui) {
				StaticSource.readFeed(tFeed);
				TraffViewer.launch(CONVERTER, DEFAULT_URL);
			} else
				System.out.println(tFeed.toXml());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void dissectFeed(LvCeliFeed feed) {
		/*
		int directionalityCount[] = {0, 0, 0};
		int dirCount[] = {0, 0, 0};
		int endpointCount[] = {0, 0, 0};
		int fallbackEventCount = 0;
		*/
		for (LvCeliMessage message : feed.messages) {
			System.out.println(String.format("Message: %s@%.3f-%.3f",
					message.acNosaukums, message.kmNo, message.kmLidz));
			dissectMessage(message);
			if (message.getOpposite() != null) {
				dissectMessage(message.getOpposite());
			}
			/*
			Boolean directionality = message.extractDirectionality();
			if (directionality == null)
				directionalityCount[0]++;
			else if (!directionality)
				directionalityCount[2]++;
			else if (directionality) {
				directionalityCount[1]++;
				Direction direction = message.extractDirection();
				if (direction == null)
					dirCount[0]++;
				else if (direction == Direction.FORWARD)
					dirCount[1]++;
				else if (direction == Direction.BACKWARD)
					dirCount[2]++;
			}
			Map<Role, Point> location = message.extractEndpoints();
			endpointCount[(location == null) ? 0 : location.containsKey(LvCeliMessage.Point.Role.AT) ? 1 : 2]++;
			List<TraffEvent> events = message.extractEvents();
			if (events.isEmpty() || ((events.size() == 1)
					&& (events.get(0).type == TraffEvent.Type.CONGESTION_TRAFFIC_PROBLEM)))
				fallbackEventCount++;
			*/
		}
		System.out.println(String.format("Messages in feed: %d", feed.messages.length));
		/*
		System.out.println(String.format("  Unidirectional: %d", directionalityCount[1]));
		System.out.println(String.format("    Forward:  %d", dirCount[1]));
		System.out.println(String.format("    Backward: %d", dirCount[2]));
		System.out.println(String.format("    Unknown:  %d", dirCount[0]));
		System.out.println(String.format("  Bidirectional:  %d", directionalityCount[2]));
		System.out.println(String.format("  Unknown:        %d", directionalityCount[0]));
		System.out.println(String.format("  Location"));
		System.out.println(String.format("    Double Point: %d", endpointCount[2]));
		System.out.println(String.format("    Single Point: %d", endpointCount[1]));
		System.out.println(String.format("    Unknown:      %d", endpointCount[0]));
		System.out.println(String.format("  Without events: %d", fallbackEventCount));
		*/
	}

	static void dissectMessage(LvCeliMessage message) {
		System.out.println(String.format("  Category: %s", message.kategorijaId));
		/*
		Boolean isUnidirectional = message.extractDirectionality();
		System.out.println(String.format("  Unidirectional: %s", isUnidirectional));
		if ((isUnidirectional != null) && isUnidirectional) {
			Direction direction = message.extractDirection();
			System.out.println(String.format("  Direction: %s", direction));
		}
		*/
		Map<TraffLocation.Point.Role, Point> location = message.extractEndpoints();
		if (location == null) {
			if ((message.latitude == null) || (message.longitude == null))
				System.out.println(String.format("  Location: km %.3f–%.3f", message.kmNo, message.kmLidz));
			else
				System.out.println(String.format("  Location: km %.3f–%.3f, see https://www.openstreetmap.org/?mlat=%f&mlon=%f",
						message.kmNo, message.kmLidz, message.latitude, message.longitude));
		} else {
			System.out.println(String.format("  Location: km %.3f–%.3f", message.kmNo, message.kmLidz));
			System.out.println(String.format("    lat/lon %.5f, %.5f", message.latitude, message.longitude));
			for (TraffLocation.Point.Role role : location.keySet()) {
				System.out.println(String.format("    %-7s %.5f, %.5f (km %.3f)", role.name(),
						location.get(role).geo.lat, location.get(role).geo.lon,
						(location.get(role).actualDistance != null) ? (location.get(role).actualDistance / 1000.0f) : null));
			}
		}
		System.out.println(String.format("  Objekta apraksts: TODO %s", message.objektaApraksts));
		System.out.println(String.format("  Ietekme uz satiksmi: %s", message.ietekmeUzSatiksmi));
		/*
		System.out.println(String.format("  Destination: %s", message.extractDestination()));
		String[] junctions = message.extractJunctions();
		System.out.println(String.format("  Junctions: %s, %s", junctions[0], junctions[1]));
		String[] enclosingJunctions = message.extractEnclosingJunctions();
		System.out.println(String.format("             %s → %s", enclosingJunctions[0], enclosingJunctions[1]));
		*/
		List<TraffEvent> events = message.extractEvents();
		System.out.println("  Events:");
		if (events.isEmpty())
			System.out.println("    (empty)");
		else
			for (TraffEvent event : events) {
				System.out.println(String.format("    %s", event.type));
				if (event.speed != null)
					System.out.println(String.format("      Speed: %d", event.speed));
				if (event.quantifier != null)
					System.out.println(String.format("      %s", event.quantifier.toAttribute()));
				for (TraffSupplementaryInfo si : event.supplementaryInfos) {
					System.out.println(String.format("      %s", si.type));
					if (si.quantifier != null)
						System.out.println(String.format("        %s", si.quantifier.toAttribute()));
				}
			}
		System.out.println("");
	}

	private static class LvCeliConverter implements ConverterSource {

		@Override
		public TraffFeed convert(File file) throws Exception {
			LvCeliFeed feed = parse(file);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(URL url) throws Exception {
			LvCeliFeed feed = parse(url);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(Properties properties) throws Exception {
			throw new UnsupportedOperationException("This operation is not implemented yet");
		}

		private TraffFeed convert(LvCeliFeed feed) {
			return new TraffFeed(feed.toTraff("test", null));
		}

		private LvCeliFeed parse(File file) throws FileNotFoundException {
			if (!file.exists()) {
				throw new IllegalArgumentException("Input file does not exist");
			} else if (!file.isFile()) {
				throw new IllegalArgumentException("Input file is not a file");
			} else if (!file.canRead()) {
				throw new IllegalArgumentException("Input file is not readable");
			}
			return LvCeliFeed.parseJson(new FileInputStream(file));
		}

		private LvCeliFeed parse(URL url) throws IOException {
			return LvCeliFeed.parseJson(url.openStream());
		}
	}
}
