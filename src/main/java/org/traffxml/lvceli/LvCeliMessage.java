/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-lvceli library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lvceli;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.lib.milestone.Distance;
import org.traffxml.lib.milestone.Milestone;
import org.traffxml.lib.milestone.MilestoneList;
import org.traffxml.lib.milestone.MilestoneUtils;
import org.traffxml.traff.DimensionQuantifier;
import org.traffxml.traff.DurationQuantifier;
import org.traffxml.traff.IntQuantifier;
import org.traffxml.traff.LatLon;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffLocation.Directionality;
import org.traffxml.traff.TraffLocation.Fuzziness;
import org.traffxml.traff.TraffLocation.RoadClass;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.traff.WeightQuantifier;

/**
 * Encapsulates a message in lvceli.lv format, equivalent to a line in a CSV file.
 */
public class LvCeliMessage {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	/**
	 * Maximum distance for clustering milestones.
	 */
	private static final float CLUSTER_DISTANCE_KM = 0.075f;

	/**
	 * Tolerance when comparing distances for consistency with the reference coordinates, in km.
	 */
	private static final float DIST_TOLERANCE_KM = 2.0f;

	/**
	 * Maximum tolerance for interpolation.
	 */
	private static final float INTERPOLATION_TOLERANCE_KM = 0.1f;

	/**
	 * Conversion from km to m, subtracting a tolerance margin.
	 */
	static final int KM_TO_M_WITH_TOLERANCE = 950;

	/**
	 * Tolerance to establish consistency or redundancy for elimination, in km.
	 */
	private static final float REDUNDANCY_TOLERANCE_KM = 0.25f;

	/**
	 * The event map with which textual descriptions in {@link #ietekmeUzSatiksmi} are parsed and
	 * converted to TraFF events.
	 * 
	 * <p>Parsing is done by means of regular expressions. First the string in {@link #ietekmeUzSatiksmi}
	 * is split into individual strings at each period or exclamation mark. Leading and trailing spaces
	 * are stripped, and multiple consecutive spaces are combined into one each. Each string is then
	 * compared to the regular expressions in {@code events.csv}.
	 * 
	 * <p>The following rules apply for regex matching:
	 * 
	 * <p>Regex matching is case-insensitive. A regex which matches {@code Brauktuves sašaurinājums} will
	 * also match {@code brauktuves sašaurinājums}.
	 * 
	 * <p>A regex must match the entire string, or exactly one element of an enumeration where elements are
	 * separated by a comma and a space. A regex which matches {@code Brauktuves sašaurinājums} will
	 * also match {@code Ātruma ierobežojums 50 km/h, brauktuves sašaurinājums, viens luksofora posms}.
	 * If a regex starts with {@code ^}, it can only match the first element in an enumeration or the
	 * entire string; if it ends with {@code $}, it can only match the last element or the entire string.  
	 * This can be used when one event string includes a comma (e.g.
	 * {@code Ātruma ierobežojumi 70, 50 un 30 km/h}) and another is matches the part before or after
	 * the comma: use {@code ^} or {@code $} on the regex for the shorter string to prevent it from
	 * matching the longer event string.
	 */
	static List<TraffEventMapping> eventMap = new ArrayList<TraffEventMapping>();

	static MilestoneList milestones;

	public final Float latitude;
	public final Float longitude;

	/**
	 * Event category.
	 * <ul>
	 * <li>1: Roadworks in progress (aktīvi būvdarbi), open to traffic but possibly with restrictions or delays</li>
	 * <li>2: Closed to traffic (slēgta satiksme), for construction works or other reasons (e.g. border closures)</li>
	 * <li>4: Roadworks planned (plānoti būvdarbi), currently no traffic impact</li>
	 * <li>9: Roadworks suspended (būvdarbi tehnoloģiskajā pārtraukumā), restrictions may still be in place</li>
	 * <li>?: Roadworks with impact on road traffic (LDZ remontdarbi, kas ietekmē satiksmi uz autoceļiem)</li>
	 * </ul>
	 */
	public final int kategorijaId;

	/**
	 * Road number.
	 */
	public final String acNosaukums;

	/**
	 * First kilometric point (usually the lower one).
	 */
	public final float kmNo;

	/**
	 * Second kilometric point (usually the higher one).
	 */
	public final float kmLidz;

	/**
	 * Description of cause.
	 */
	public final String objektaApraksts;

	/**
	 * Traffic impact (can describe multiple events).
	 */
	public final String ietekmeUzSatiksmi;

	/**
	 * The time at which the current update for this event was received.
	 */
	private final Date updateTime = new Date();

	/* Message for the same event in the opposite direction, if any */
	private LvCeliMessage opposite = null;

	/* cached data */
	private Map <TraffLocation.Point.Role, Point> endpoints = null;

	static {
		/* Read milestone data set */
		try {
			milestones = new MilestoneList(LvCeliMessage.class.getResourceAsStream("milestones.csv"),
					',', null, "Latitude", "Longitude", "AC_INDEX", "KM", Distance.UNIT_KM);
		} catch (IOException e) {
			LOG.debug("Could not read milestones", e);
		}
		/* Read event data set */
		try {
			InputStreamReader reader = new InputStreamReader(new BOMInputStream(LvCeliMessage.class.getResourceAsStream("events.csv")));
			Iterable<CSVRecord> records;
			records = CSVFormat.DEFAULT.withDelimiter('\t').withFirstRecordAsHeader().parse(reader);
			for (CSVRecord record : records)
				try {
					String traffEvent = record.get("traffEvent");
					if (traffEvent.isEmpty())
						continue;
					String regex = record.get("regex");
					String attrFunc = record.get("attrFunc");
					String attrType = record.get("attrType");
					String attrValue = record.get("attrValue");
					String siType = record.get("siType");
					eventMap.add(new TraffEventMapping(regex, traffEvent, attrFunc, attrType, attrValue, siType));
				} catch (Exception e) {
					e.printStackTrace(); // FIXME debug
					continue;
				}
		} catch (IOException e) {
			LOG.debug("Could not read events", e);
		}
	}

	/**
	 * Returns the earlier of two dates.
	 * 
	 * <p>If one argument is null, the other is returned. If both are null, null is returned.
	 * 
	 * @param lhs The first date
	 * @param rhs The second date
	 * @return The earlier of the two dates, or the only non-null date, or null if both dates are null
	 */
	private static Date earlier(Date lhs, Date rhs) {
		if (lhs == null)
			return rhs;
		if (rhs == null)
			return lhs;
		if (lhs.before(rhs))
			return lhs;
		else
			return rhs;
	}

	private List<TraffEvent> getEventsFromIetekme() {
		List<TraffEvent> res = new ArrayList<TraffEvent>();
		String[] ietekmes = ietekmeUzSatiksmi.split("[\\.!]");
		for (String ietekme : ietekmes) {
			/* normalize whitespaces */
			ietekme = ietekme.trim().replaceAll(" +", " ");
			for (TraffEventMapping mapping : eventMap) {
				TraffEvent event = mapping.getEvent(ietekme);
				if (event != null)
					res.add(event);
			}
		}
		if (res.isEmpty())
			System.err.println("No events for: " + ietekmeUzSatiksmi);
		return res;
	}

	/**
	 * Returns the later of two dates.
	 * 
	 * <p>If one argument is null, the other is returned. If both are null, null is returned.
	 * 
	 * @param lhs The first date
	 * @param rhs The second date
	 * @return The later of the two dates, or the only non-null date, or null if both dates are null
	 */
	private static Date later(Date lhs, Date rhs) {
		if (lhs == null)
			return rhs;
		if (rhs == null)
			return lhs;
		if (lhs.after(rhs))
			return lhs;
		else
			return rhs;
	}

	static LvCeliMessage fromJson(JSONObject object) {
		Float latitude = null;
		Float longitude = null;
		int kategorijaId = Integer.MIN_VALUE;
		String acNosaukums = null;
		float kmNo = Float.NaN;
		float kmLidz = Float.NaN;
		String objektaApraksts = null;
		String ietekmeUzSatiksmi = null;

		latitude = (float) object.optDouble("Latitude");
		if (latitude == Float.NaN)
			latitude = null;
		longitude = (float) object.optDouble("Longitude");
		if (longitude == Float.NaN)
			longitude = null;
		kategorijaId = object.optInt("KATEGORIJA_ID", Integer.MIN_VALUE);
		if (kategorijaId == Integer.MIN_VALUE)
			return null;
		acNosaukums = object.optString("AC_NOSAUKUMS", null);
		kmNo = (float) object.optDouble("KM_NO");
		if (kmNo == Float.NaN)
			return null;
		kmLidz = (float) object.optDouble("KM_LIDZ");
		if (kmLidz == Float.NaN)
			return null;
		objektaApraksts = object.optString("OBJEKTA_APRAKSTS", null);
		ietekmeUzSatiksmi = object.optString("IETEKME_UZ_SATIKSMI", null);

		return new LvCeliMessage(latitude, longitude, kategorijaId, acNosaukums, kmNo, kmLidz,
				objektaApraksts, ietekmeUzSatiksmi);
	}

	public LvCeliMessage(Float latitude, Float longitude, int kategorijaId, String acNosaukums, float kmNo,
			float kmLidz, String objektaApraksts, String ietekmeUzSatiksmi) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.kategorijaId = kategorijaId;
		this.acNosaukums = acNosaukums;
		this.kmNo = kmNo;
		this.kmLidz = kmLidz;
		this.objektaApraksts = objektaApraksts;
		this.ietekmeUzSatiksmi = ietekmeUzSatiksmi;
	}

	public LvCeliMessage(CSVRecord record) {
		super();

		Float latitude = null;
		Float longitude = null;

		if (record.isMapped("Latitude"))
			try {
				latitude = Float.valueOf(record.get("Latitude"));
			} catch (Exception e) {
				// NOP
			}
		this.latitude = latitude;
		if (record.isMapped("Longitude"))
			try {
				longitude = Float.valueOf(record.get("Longitude"));
			} catch (Exception e) {
				// NOP
			}
		this.longitude = longitude;
		kategorijaId = Integer.valueOf(record.get("KATEGORIJA_ID").trim());

		String acNosaukums = record.get("AC_NOSAUKUMS");
		acNosaukums = acNosaukums.trim();
		if ("-".equals(acNosaukums) || acNosaukums.isEmpty())
			acNosaukums = null;
		this.acNosaukums = acNosaukums;

		kmNo = Float.valueOf(record.get("KM_NO"));
		kmLidz = Float.valueOf(record.get("KM_LIDZ"));
		if (record.isMapped("OBJEKTA_APRAKSTS"))
			objektaApraksts = record.get("OBJEKTA_APRAKSTS");
		else
			objektaApraksts = null;
		if (record.isMapped("IETEKME_UZ_SATIKSMI"))
			ietekmeUzSatiksmi = record.get("IETEKME_UZ_SATIKSMI");
		else
			ietekmeUzSatiksmi = null;
	}

	/**
	 * Retrieves endpoint coordinates from a message.
	 * 
	 * <p>If no suitable endpoints are found, the result is null. Otherwise, the result is a map of endpoints
	 * and their roles.
	 * 
	 * <p>Roles always refer to the forward direction of the road, i.e. the distance indications for the FROM,
	 * AT and TO points will always be in ascending order. For locations referring to the backward direction
	 * of the road, the FROM and TO roles would need to be swapped.
	 * 
	 * <p>The caller should not make any assumptions on the number of points returned: a zero-length location
	 * may be resolved to the two location markers enclosing it. Conversely, a location with a nonzero length
	 * may be resolved to the only known location marker located in its vicinity.
	 * 
	 * @return
	 */
	public Map<TraffLocation.Point.Role, Point> extractEndpoints() {
		if (endpoints != null)
			return endpoints;
		endpoints = new HashMap<TraffLocation.Point.Role, Point>();
		if (kmNo == kmLidz) {
			if (opposite == null) {
				// zero length but unidirectional (without direction information), use lat/lon as given in message
				endpoints.put(TraffLocation.Point.Role.AT, new Point(kmNo * 1000, new LatLon(latitude, longitude), kmNo * 1000));
				return endpoints;
			} else if ((latitude == opposite.latitude) && (longitude == opposite.longitude)) {
				// bidirectional but zero length by all means, get enclosing points
				endpoints.put(TraffLocation.Point.Role.AT, new Point(kmNo * 1000, new LatLon(latitude, longitude), kmNo * 1000));
				Distance distance = new Distance(kmNo, Distance.UNIT_KM);
				Map<Distance, Set<Milestone>> enclosing = MilestoneUtils.clusterWithin(milestones.getEnclosing(acNosaukums, distance),
						CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
				// TODO eliminate from enclosing
				for (Map.Entry<Distance, Set<Milestone>> e : enclosing.entrySet()) {
					if (e.getValue().size() != 1)
						continue;
					float eDistM = e.getKey().asUnit(Distance.UNIT_M);
					for (Milestone milestone : e.getValue())
						endpoints.put((eDistM < kmNo) ? TraffLocation.Point.Role.FROM : TraffLocation.Point.Role.TO,
								new Point(eDistM, milestone.coords, eDistM));
				}
				return endpoints;
			} else {
				// nominally zero length, but two different endpoints
				endpoints.put(TraffLocation.Point.Role.FROM, new Point(kmNo * 1000, new LatLon(latitude, longitude), kmNo * 1000));
				endpoints.put(TraffLocation.Point.Role.TO, new Point(kmNo * 1000, new LatLon(opposite.latitude, opposite.longitude), kmNo * 1000));
			}
		} else if (opposite != null) {
			// nonzero length, different endpoints: match distances to endpoints
			LatLon geo1 = new LatLon(latitude, longitude);
			LatLon geo2 = new LatLon(opposite.latitude, opposite.longitude);
			Distance dFrom = new Distance(kmNo, Distance.UNIT_KM);
			Distance dTo = new Distance(kmLidz, Distance.UNIT_KM);
			Set<Milestone> fromFloor = MilestoneUtils.clusterWithin(milestones.getFloor(acNosaukums, dFrom),
					CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
			Set<Milestone> toCeiling = MilestoneUtils.clusterWithin(milestones.getCeiling(acNosaukums, dTo),
					CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
			// TODO eliminate from mFrom and mTo
			double from1 = 40000000;
			double from2 = 40000000;
			double to1 = 40000000;
			double to2 = 40000000;
			// FIXME subtract difference in nominal distance from error calculations
			for (Milestone m : fromFloor) {
				from1 = Math.min(from1, geo1.distanceTo(m.coords));
				from2 = Math.min(from2, geo2.distanceTo(m.coords));
			}
			for (Milestone m : toCeiling) {
				to1 = Math.min(to1, geo1.distanceTo(m.coords));
				to2 = Math.min(to2, geo2.distanceTo(m.coords));
			}
			double errFromTo = from1 + to2;
			double errToFrom = to1 + from2;
			if (errFromTo < errToFrom) {
				endpoints.put(TraffLocation.Point.Role.FROM, new Point(kmNo * 1000, geo1, kmNo * 1000));
				endpoints.put(TraffLocation.Point.Role.TO, new Point(kmLidz * 1000, geo2, kmLidz * 1000));
			} else if (errFromTo > errToFrom) {
				endpoints.put(TraffLocation.Point.Role.FROM, new Point(kmNo * 1000, geo2, kmNo * 1000));
				endpoints.put(TraffLocation.Point.Role.TO, new Point(kmLidz * 1000, geo1, kmLidz * 1000));
			} else {
				// match failed (usually when milestone lookup fails)
				endpoints.put(TraffLocation.Point.Role.FROM, new Point(null, geo1, null));
				endpoints.put(TraffLocation.Point.Role.TO, new Point(null, geo2, null));
			}
		} else {
			// nonzero length, unidirectional: one endpoint needs to be derived from a milestone
			LatLon geo = new LatLon(latitude, longitude);
			Distance dFrom = new Distance(kmNo, Distance.UNIT_KM);
			Distance dTo = new Distance(kmLidz, Distance.UNIT_KM);
			Set<Milestone> fromFloor = MilestoneUtils.clusterWithin(milestones.getFloor(acNosaukums, dFrom),
					CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
			Set<Milestone> toCeiling = MilestoneUtils.clusterWithin(milestones.getCeiling(acNosaukums, dTo),
					CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
			// TODO eliminate from mFrom and mTo
			double eFrom = 40000000;
			double eTo = 40000000;
			// FIXME subtract difference in nominal distance from error calculations
			for (Milestone m : fromFloor)
				eFrom = Math.min(eFrom, geo.distanceTo(m.coords));
			for (Milestone m : toCeiling)
				eTo = Math.min(eTo, geo.distanceTo(m.coords));
			if (eFrom < eTo) {
				// geo refers to from, need to find to
				endpoints.put(TraffLocation.Point.Role.FROM, new Point(kmNo * 1000, geo, kmNo * 1000));
				NavigableMap<Distance, Set<Milestone>> toEOP = MilestoneUtils.clusterWithin(milestones.get(acNosaukums),
						CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
				MilestoneUtils.eliminateRedundantBetweenSides(toEOP, dTo, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM, true);
				MilestoneUtils.interpolate(toEOP, dTo, INTERPOLATION_TOLERANCE_KM, Distance.UNIT_KM);
				toEOP = MilestoneUtils.eliminate(toEOP, geo, Math.abs(kmLidz - kmNo) + DIST_TOLERANCE_KM, Distance.UNIT_KM);
				Set<Milestone> toNearest = toEOP.get(dTo);
				if ((toNearest == null) || (toNearest.isEmpty()))
					toNearest = MilestoneUtils.clusterWithin(milestones.getNearest(acNosaukums, dTo),
							CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
				boolean success = false;
				if (toNearest.size() == 1)
					for (Milestone milestone : toNearest) {
						float dist = milestone.distance.asUnit(Distance.UNIT_KM);
						if (dist > kmNo) {
							endpoints.put(TraffLocation.Point.Role.TO,
									new Point(kmLidz * 1000, milestone.coords, kmLidz * 1000));
							success = true;
						}
					}
				// the point closest to kmLidz lies before kmNo, use ceiling
				if ((!success) && (toCeiling.size() == 1))
					for (Milestone milestone : toCeiling) {
						float dist = milestone.distance.asUnit(Distance.UNIT_KM);
						endpoints.put(TraffLocation.Point.Role.TO,
								new Point(kmLidz * 1000, milestone.coords, kmLidz * 1000));
					}
			} else if (eFrom > eTo) {
				// geo refers to to, need to find from
				endpoints.put(TraffLocation.Point.Role.TO, new Point(kmLidz * 1000, geo, kmLidz * 1000));
				NavigableMap<Distance, Set<Milestone>> fromEOP = MilestoneUtils.clusterWithin(milestones.get(acNosaukums),
						CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
				MilestoneUtils.eliminateRedundantBetweenSides(fromEOP, dFrom, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM, true);
				MilestoneUtils.interpolate(fromEOP, dFrom, INTERPOLATION_TOLERANCE_KM, Distance.UNIT_KM);
				fromEOP = MilestoneUtils.eliminate(fromEOP, geo, Math.abs(kmLidz - kmNo) + DIST_TOLERANCE_KM, Distance.UNIT_KM);
				Set<Milestone> fromNearest = fromEOP.get(dFrom);
				if ((fromNearest == null) || (fromNearest.isEmpty()))
					fromNearest = MilestoneUtils.clusterWithin(milestones.getNearest(acNosaukums, dFrom),
							CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
				boolean success = false;
				if (fromNearest.size() == 1)
					for (Milestone milestone : fromNearest) {
						float dist = milestone.distance.asUnit(Distance.UNIT_KM);
						if (dist < kmLidz) {
							endpoints.put(TraffLocation.Point.Role.FROM,
									new Point(kmNo * 1000, milestone.coords, kmNo * 1000));
							success = true;
						}
					}
				// the point closest to kmNo lies after kmLidz, use floor
				if ((!success) && (fromFloor.size() == 1))
					for (Milestone milestone : fromFloor) {
						float dist = milestone.distance.asUnit(Distance.UNIT_KM);
						endpoints.put(TraffLocation.Point.Role.FROM,
								new Point(kmNo * 1000, milestone.coords, kmNo * 1000));
					}
			} else {
				// match failed (usually when milestone lookup fails), need to find both
				// TODO
			}
		}

		return endpoints;
	}

	/**
	 * Retrieves TraFF events from the message.
	 */
	public List<TraffEvent> extractEvents() {
		List<TraffEvent> res = new ArrayList<TraffEvent>();
		TraffEvent.Builder builder;
		switch(kategorijaId) {
		// TODO LDZ remontdarbi, kas ietekmē satiksmi uz autoceļiem; roadworks with traffic impact
		case 1: // aktīvi būvdarbi, roadworks in progress
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.CONSTRUCTION);
			builder.setType(TraffEvent.Type.CONSTRUCTION_CONSTRUCTION_WORK);
			res.add(builder.build());
			res.addAll(getEventsFromIetekme());
			break;
		case 2: // slēgta satiksme, closures
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_CLOSED);
			res.add(builder.build());
			break;
		case 4: // plānoti būvdarbi, planned roadworks
			// NOP
			break;
		case 9: // būvdarbi tehnoloģiskajā pārtraukumā, suspended roadworks
			res.addAll(getEventsFromIetekme());
			break;
		default:
			res.addAll(getEventsFromIetekme());
		}
		return res;
	}

	public String getId() {
		if (acNosaukums != null)
			return String.format("%s@%.3f-%.3f", acNosaukums, kmNo, kmLidz);
		if ((latitude != null) && (longitude != null))
			return String.format("%.5f,%.5f", latitude, longitude);
		return null;
	}

	/**
	 * Returns the corresponding message for the opposite direction, if any.
	 * 
	 * <p>All public members of the message, with the exception of {@link #latitude} and {@link #longitude},
	 * hold equal values (i.e. {@code a = b} holds true for primitives, {@code a.equals(b)} for
	 * class instances).
	 * 
	 * @return The event for the opposite direction, or NULL if none exists.
	 */
	public LvCeliMessage getOpposite() {
		return opposite;
	}

	/**
	 * Joins two messages together if they refer to the same event but in the opposite direction.
	 * 
	 * <p>In order to join two messages together, neither of the two messages may already be joined to
	 * a different message (i.e. {@link #getOpposite()} must not return a non-NULL value which differs
	 * from the other message) and of the public members of the two messages, only {@link #latitude} and
	 * {@link #longitude} may differ. For all other public members, {@code a = b} must holds true for
	 * primitives, {@code a.equals(b)} for class instances.
	 * 
	 * <p>Joining two messages which are already joined together is a no-op and will return true.
	 * 
	 * @param that The other message
	 * 
	 * @return True of the join was successful (including if the two messages were already joined together), false otherwise.
	 */
	public boolean join(LvCeliMessage that) {
		if ((this.getOpposite() == that) && (that.getOpposite() == this))
			return true;
		if ((this.getOpposite() != null) || (that.getOpposite() != null))
			return false;
		if ((this.kategorijaId != that.kategorijaId) || (this.kmNo != that.kmNo)
				|| (this.kmLidz != that.kmLidz))
			return false;
		if (!(((this.acNosaukums == null) && (that.acNosaukums == null))
				|| this.acNosaukums.equals(that.acNosaukums)))
			return false;
		if (!(((this.objektaApraksts == null) && (that.objektaApraksts == null))
				|| this.objektaApraksts.equals(that.objektaApraksts)))
			return false;
		if (!(((this.ietekmeUzSatiksmi == null) && (that.ietekmeUzSatiksmi == null))
				|| this.ietekmeUzSatiksmi.equals(that.ietekmeUzSatiksmi)))
			return false;
		this.opposite = that;
		that.opposite = this;
		return true;
	}

	/**
	 * Converts the message to a TraFF message.
	 * 
	 * @param sourcePrefix The source prefix to be used for the ID
	 * @param oldMessages A previous feed which may hold the previous version of this message
	 * 
	 * @return A TraFF message whose contents represent this message.
	 */
	public TraffMessage toTraff(String sourcePrefix, Collection<TraffMessage> oldMessages) {
		TraffMessage.Builder builder = new TraffMessage.Builder();
		String id = String.format("%s:%s", sourcePrefix, getId());
		builder.setId(id);

		/*
		 * Timestamps:
		 * Receive time is the earliest recorded timestamp (including receive times of existing messages with
		 * the same ID)
		 * Update time is that of the feed
		 * Expiration time is 24 hours, or 86,400,000 milliseconds, after receive time
		 * Start/end time are not used
		 */
		builder.setUpdateTime(updateTime);
		Date receiveTime = updateTime;
		Date expirationTime = new Date(updateTime.getTime() + 86400000);

		/* enforce consistency with previous messages:
		 * receive time is the earliest timestamp observed
		 * expirationTime is the latest timestamp observed
		 */
		if (oldMessages != null)
			for (TraffMessage message : oldMessages)
				if (id.equals(message.id)) {
					receiveTime = earlier(receiveTime, message.receiveTime);
					expirationTime = later(expirationTime, message.expirationTime);
				}
		builder.setReceiveTime(receiveTime);
		builder.setUpdateTime(updateTime);
		builder.setExpirationTime(expirationTime);

		/* Assemble the location */
		TraffLocation.Builder locBuilder = new TraffLocation.Builder();
		locBuilder.setDirectionality((opposite == null) ? Directionality.ONE_DIRECTION : Directionality.BOTH_DIRECTIONS);
		locBuilder.setCountry("LV");
		/* territory (which would be novads) and town data are not supplied in an easy-to-parse form */
		// TODO origin and destination (which would need to be obtained from another data source)
		locBuilder.setFuzziness(Fuzziness.MEDIUM_RES);
		locBuilder.setRoadRef(acNosaukums);
		if (acNosaukums != null) {
			if (acNosaukums.startsWith("A"))
				locBuilder.setRoadClass(RoadClass.TRUNK);
			else if (acNosaukums.startsWith("P"))
				locBuilder.setRoadClass(RoadClass.PRIMARY);
			else if (acNosaukums.startsWith("V"))
				locBuilder.setRoadClass(RoadClass.SECONDARY);
		}
		Map<TraffLocation.Point.Role, Point> endpoints = extractEndpoints();
		if (endpoints == null)
			// TODO error message?
			return null;
		for (Map.Entry<TraffLocation.Point.Role, Point> endpoint : endpoints.entrySet()) {
			TraffLocation.Point.Builder pBuilder = new TraffLocation.Point.Builder();
			pBuilder.setCoordinates(endpoint.getValue().geo);
			if (endpoint.getValue().nominalDistance != null)
				pBuilder.setDistance(endpoint.getValue().nominalDistance / 1000.0f);
			locBuilder.setPoint(endpoint.getKey(), pBuilder.build());
		}
		// FIXME can we have backward directions? Then: locBuilder.invert();
		try {
			builder.setLocation(locBuilder.build());

			/* Events */
			builder.addEvents(extractEvents());

			return builder.build();
		} catch (IllegalStateException e) {
			// TODO error message?
			return null;
		}
	}

	/**
	 * Represents one of the points which defines a traffic location.
	 */
	public static class Point {
		/**
		 * The coordinates of the point.
		 */
		public final LatLon geo;

		/**
		 * The kilometric point to which this point refers, in meters.
		 * 
		 * <p>The nominal kilometric point is taken directly from the feed, without any further adaptations. It
		 * may differ from the location indicated by {@link #geo}.
		 */
		public final Float nominalDistance;

		/**
		 * The kilometric point to which {@link #geo} refers, in meters.
		 * 
		 * <p>This may differ slightly from {@link #nominalDistance}, the nominal distance indicated in the
		 * feed. Typically, it is rounded to the nearest multiple of 1 km; more generically, it represents
		 * the kilometric point closest to {@link #nominalDistance}. 
		 */
		public final Float actualDistance;

		private Point(Float nominalDistance, LatLon geo, Float actualDistance) {
			super();
			this.nominalDistance = nominalDistance;
			this.geo = geo;
			this.actualDistance = actualDistance;
		}
	}

	/**
	 * A mapping which specifies how a Datex-2 Enum value maps to one or multiple TraFF events.
	 * 
	 * <p>If a single value is represented by multiple TraFF events, one {@code TraffEventMapping} instance
	 * exists for each TraFF event.
	 */
	private static class TraffEventMapping {
		/**
		 * Functions to interpret values in {@code attrValue}.
		 */
		private static enum AttrFunc {
			/** Averages over the values in the given matcher groups */
			AVG,
			/** Interprets the value as a duration, with hours specified in the matcher group at the first index and minutes at the second index */
			HH_MM,
			/** Takes the value of the given matcher group without further modification */
			INDEX,
			/** Interprets the value as a literal number rather than a matcher group */
			LITERAL,
			/** Returns the smallest value in any of the given matcher groups */
			MIN
		};

		static final Pattern TIME_PATTERN = Pattern.compile(":");

		/** The pattern for the event description. */
		public final Pattern pattern;

		/** The TraFF event class. */
		public final TraffEvent.Class traffClass;

		/** The TraFF event. */
		public final TraffEvent.Type traffEvent;

		/** How to interpret the value in attrValue. */
		public final AttrFunc attrFunc;

		/** The attribute type (speed or a quantifier type). */
		public final String attrType;

		/** The attribute value (a literal value of a comma-separated list of indices). */
		public final String attrValue;

		/** The TraFF supplementary information class. */
		public final TraffSupplementaryInfo.Class siClass;

		/** The TraFF supplementary information type. */
		public final TraffSupplementaryInfo.Type siType;

		/**
		 * Determines the TraFF event class for a given TraFF event type.
		 * 
		 * <p>This is done based on the convention that event types begin with the event class, followed by an
		 * underscore. If the string representation of the event type does not match that of a known event class,
		 * the last underscore and all characters following it are dropped. This is repeated until a match is
		 * found or the remaining string does not contain any more underscores. The first match is returned,
		 * which is equivalent to the “maximum munch” rule: If we were to have an event class named {@code FOO}
		 * and another named {@code FOO_BAR}, then an event type {@code FOO_BAR_BAZ} would yield {@code FOO_BAR},
		 * not {@code FOO}.
		 * 
		 * @param type The event type
		 * @return The event class, or null if no match was found (including if {@code type} is null)
		 */
		private static TraffEvent.Class getTraffEventClass(TraffEvent.Type type) {
			if (type == null)
				return null;
			TraffEvent.Class res = null;
			String name = type.name();
			while (res == null) {
				try {
					res = Enum.valueOf(TraffEvent.Class.class, name);
					break;
				} catch (IllegalArgumentException e) {
					if (!name.contains("_"))
						break;
					name = name.substring(0, name.lastIndexOf("_"));
				}
			}
			return res;
		}

		/**
		 * Determines the TraFF supplementary information class for a given TraFF SI type.
		 * 
		 * <p>This is done based on the convention that SI types begin with {@code S_}, followed by the event class
		 * and an underscore. If the string representation of the event type does not match that of a known event
		 * class, the last underscore and all characters following it are dropped. This is repeated until a match
		 * is found or the remaining string does not contain any more underscores. The first match is returned,
		 * which is equivalent to the “maximum munch” rule: If we were to have an event class named {@code FOO}
		 * and another named {@code FOO_BAR}, then an event type {@code FOO_BAR_BAZ} would yield {@code FOO_BAR},
		 * not {@code FOO}.
		 * 
		 * @param type The event type
		 * @return The event class, or null if no match was found (including if {@code type} is null)
		 */
		private static TraffSupplementaryInfo.Class getTraffSiClass(TraffSupplementaryInfo.Type type) {
			if (type == null)
				return null;
			String name = type.name();
			if (name.startsWith("S_"))
				name = name.substring(2);
			else
				return null;
			TraffSupplementaryInfo.Class res = null;
			while (res == null) {
				try {
					res = Enum.valueOf(TraffSupplementaryInfo.Class.class, name);
					break;
				} catch (IllegalArgumentException e) {
					if (!name.contains("_"))
						break;
					name = name.substring(0, name.lastIndexOf("_"));
				}
			}
			return res;
		}

		TraffEventMapping(String regex, String traffEvent, String attrFunc, String attrType, String attrValue, String siType)
				throws IllegalArgumentException {
			this.pattern = Pattern.compile("(?:.*, )?" + regex + "(, .*)?", Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
			this.traffEvent = Enum.valueOf(TraffEvent.Type.class, traffEvent);
			this.traffClass = getTraffEventClass(this.traffEvent);
			if (attrFunc.isEmpty())
				this.attrFunc = null;
			else
				this.attrFunc = Enum.valueOf(AttrFunc.class, attrFunc);
			if (attrType.isEmpty())
				this.attrType = null;
			else
				this.attrType = attrType;
			if (attrValue.isEmpty())
				this.attrValue = null;
			else
				this.attrValue = attrValue;
			if (siType.isEmpty()) {
				this.siType = null;
				this.siClass = null;
			} else {
				this.siType = Enum.valueOf(TraffSupplementaryInfo.Type.class, siType);
				this.siClass = getTraffSiClass(this.siType);
			}
		}

		TraffEvent getEvent(String desc) {
			Matcher matcher = pattern.matcher(desc);
			if (!matcher.matches())
				return null;
			TraffEvent.Builder builder = new TraffEvent.Builder();
			builder.setEventClass(traffClass);
			builder.setType(traffEvent);
			if ((attrFunc != null) && (attrType != null) && (attrValue != null)) {
				String[] attrValues = attrValue.split(", *");
				if (attrFunc != AttrFunc.LITERAL)
					/* iteration has to be by index so we can modify the items */
					for (int i = 0; i < attrValues.length; i++)
						/* replace decimal separator while we read the values */
						attrValues[i] = matcher.group(Integer.valueOf(attrValues[i])).replace(",", ".");
				Float value = null;
				switch (attrFunc) {
				case AVG:
					value = 0f;
					for (String val : attrValues)
						value += Float.valueOf(val);
					if (attrValues.length > 0)
						value /= attrValues.length;
					else
						value = null;
					break;
				case HH_MM:
					if (attrValues.length == 1)
						value = Float.valueOf(attrValues[0]);
					else if (attrValues.length == 2)
						value = Float.valueOf(attrValues[0]) * 60 + Float.valueOf(attrValues[1]);
					break;
				case INDEX:
				case LITERAL:
					value = Float.valueOf(attrValues[0]);
					break;
				case MIN:
					for (String val : attrValues)
						if (value == null)
							value = Float.valueOf(val);
						else
							value = Math.min(value, Float.valueOf(val));
					break;
				}
				if (value != null) {
					if ("speed".equals(attrType))
						builder.setSpeed(value.intValue());
					else if ("q_dimension".equals(attrType))
						builder.setQuantifier(new DimensionQuantifier(value));
					else if ("q_duration".equals(attrType))
						builder.setQuantifier(new DurationQuantifier(value.intValue()));
					else if ("q_int".equals(attrType))
						builder.setQuantifier(new IntQuantifier(value.intValue()));
					else if ("q_weight".equals(attrType))
						builder.setQuantifier(new WeightQuantifier(value));
					else
						// TODO add support for more quantifier types
						throw new IllegalArgumentException("Unsupported attribute type: '" + attrType + "'");
				}
			}
			if (siType != null)
				builder.addSupplementaryInfo(new TraffSupplementaryInfo(siClass, siType));
			return builder.build();
		}
	}

	/**
	 * A mapping which specifies how a value maps to one or multiple TraFF supplementary information items.
	 * 
	 * <p>If a single value is represented by multiple TraFF SI items, one {@code TraffSiMapping} instance
	 * exists for each TraFF SI item.
	 */
	private static class TraffSiMapping {

		/** The TraFF supplementary information class */
		public final TraffSupplementaryInfo.Class siClass;

		/** The TraFF supplementary information type */
		public final TraffSupplementaryInfo.Type siType;

		/**
		 * Determines the TraFF supplementary information class for a given TraFF SI type.
		 * 
		 * <p>This is done based on the convention that SI types begin with {@code S_}, followed by the event class
		 * and an underscore. If the string representation of the event type does not match that of a known event
		 * class, the last underscore and all characters following it are dropped. This is repeated until a match
		 * is found or the remaining string does not contain any more underscores. The first match is returned,
		 * which is equivalent to the “maximum munch” rule: If we were to have an event class named {@code FOO}
		 * and another named {@code FOO_BAR}, then an event type {@code FOO_BAR_BAZ} would yield {@code FOO_BAR},
		 * not {@code FOO}.
		 * 
		 * @param type The event type
		 * @return The event class, or null if no match was found (including if {@code type} is null)
		 */
		private static TraffSupplementaryInfo.Class getTraffSiClass(TraffSupplementaryInfo.Type type) {
			if (type == null)
				return null;
			String name = type.name();
			if (name.startsWith("S_"))
				name = name.substring(2);
			else
				return null;
			TraffSupplementaryInfo.Class res = null;
			while (res == null) {
				try {
					res = Enum.valueOf(TraffSupplementaryInfo.Class.class, name);
					break;
				} catch (IllegalArgumentException e) {
					if (!name.contains("_"))
						break;
					name = name.substring(0, name.lastIndexOf("_"));
				}
			}
			return res;
		}

		TraffSiMapping(String type) throws IllegalArgumentException {
			super();
			this.siType = Enum.valueOf(TraffSupplementaryInfo.Type.class, type);
			this.siClass = getTraffSiClass(this.siType);
		}
	}
}
