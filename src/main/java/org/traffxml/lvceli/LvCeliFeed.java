/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-lvceli library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lvceli;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffMessage;

/**
 * Encapsulates a feed in CSV format, as used by lvceli.lv.
 */
public class LvCeliFeed {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	/**
	 * Messages in the feed.
	 */
	public final LvCeliMessage[] messages;

	public LvCeliFeed(LvCeliMessage[] messages) {
		super();
		this.messages = messages;
	}

	/**
	 * Parses a feed in lvceli.lv JSON format.
	 * 
	 * @param stream A stream containing the JSON data
	 * @return A feed containing the data from the stream
	 */
	public static LvCeliFeed parseJson(InputStream stream) {
		List<LvCeliMessage> messages = new ArrayList<LvCeliMessage>();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		int i;

		try {
			i = stream.read();
			while (i != -1) {
				if (i >= 32) outputStream.write(i);
				i = stream.read();
			}
			stream.close();
		} catch (IOException e) {
			LOG.debug("{}", e);
			System.exit(1);
		}

		JSONTokener tokener = new JSONTokener(outputStream.toString());
		Object object = null;
		try {
			object = tokener.nextValue();
			if (!(object instanceof JSONObject)) {
				LOG.warn("Got instance of {} at top level, expected JSONObject",
						object.getClass().getName());
				return new LvCeliFeed(new LvCeliMessage[]{});
			}
			Object result = ((JSONObject) object).opt("result");
			if (!(result instanceof JSONObject)) {
				LOG.warn("Got instance of {} for result, expected JSONObject",
						result.getClass().getName());
				return new LvCeliFeed(new LvCeliMessage[]{});
			}
			Object records = ((JSONObject) result).opt("records");
			if (!(records instanceof JSONArray)) {
				LOG.warn("Got instance of {} for result.records, expected JSONArray",
						records.getClass().getName());
				return new LvCeliFeed(new LvCeliMessage[]{});
			}
			for (i = 0; i < ((JSONArray) records).length(); i++) {
				Object child;
				try {
					child = ((JSONArray) records).get(i);
				} catch (JSONException e) {
					LOG.warn("Failed to retrieve object #{} from top-level array", i);
					LOG.debug("{}", e);
					continue;
				}
				if (child instanceof JSONObject) {
					messages.add(LvCeliMessage.fromJson((JSONObject) child));
				} else
					LOG.warn("Got instance of {} in result.records array, expected JSONObject",
							child.getClass().getName());
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return new LvCeliFeed(messages.toArray(new LvCeliMessage[]{}));
	}

	/**
	 * Parses a feed in lvceli.lv CSV format.
	 * 
	 * @param stream A stream containing the CSV data
	 * @return A feed containing the data from the stream
	 */
	public static LvCeliFeed parseCsv(InputStream stream) {
		Map<String, Collection<LvCeliMessage>> msgMap = new HashMap<String, Collection<LvCeliMessage>>();
		List<LvCeliMessage> messages = new ArrayList<LvCeliMessage>();
		InputStreamReader reader = new InputStreamReader(new BOMInputStream(stream));
		Iterable<CSVRecord> records;
		try {
			records = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader);
			for (CSVRecord record : records)
				try {
					LvCeliMessage message = new LvCeliMessage(record);
					String id = message.getId();
					Collection<LvCeliMessage> candidates = msgMap.get(id);
					if (candidates != null) {
						boolean joined = false;
						for (LvCeliMessage message2 : msgMap.get(id))
							if (message.join(message2)) {
								joined = true;
								break;
							}
						if (!joined)
							candidates.add(message);
					} else {
						Collection<LvCeliMessage> list = new ArrayList<LvCeliMessage>();
						list.add(message);
						msgMap.put(id, list);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			try {
				reader.close();
			} catch (IOException e) {
				// NOP
			}
			for (Collection<LvCeliMessage> list : msgMap.values())
				messages.addAll(list);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new LvCeliFeed(messages.toArray(new LvCeliMessage[]{}));
	}

	/**
	 * Converts the feed to a list of TraFF messages.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument, in order for
	 * information from those messages to be used in the feed: The result will hold cancellation messages for
	 * all message IDs in {@code oldMessages} which are not matched by a message in this feed. If messages in
	 * this feed are matched by an item in {@code oldMessages}, the new message will inherit the receive time
	 * of the old one.
	 * 
	 * <p>If messages in {@code oldMessages} have an ID which starts with a source prefix other than
	 * {@code sourcePrefix}, these messages are effectively ignored and no cancellations are generated for
	 * them.
	 * 
	 * @param sourcePrefix The source prefix to use for comparison
	 * @param oldMessages Previously received messages, can be null
	 */
	public List<TraffMessage> toTraff(String sourcePrefix, Collection<TraffMessage> oldMessages) {
		Date now = new Date();
		Map<String, TraffMessage> outMap = new HashMap<String, TraffMessage>();
		for (LvCeliMessage inMessage: messages) {
			TraffMessage outMessage = inMessage.toTraff(sourcePrefix, oldMessages);
			if (outMessage != null)
				outMap.put(outMessage.id, outMessage);
		}
		/* Add cancellations for all unmatched old messages */
		if (oldMessages != null)
			for (TraffMessage oldMessage : oldMessages)
				if (oldMessage.id.startsWith(sourcePrefix + ":")
						&& !outMap.containsKey(oldMessage.id)) {
					TraffMessage.Builder builder = new TraffMessage.Builder();
					builder.setId(oldMessage.id);
					builder.setReceiveTime(oldMessage.receiveTime);
					builder.setUpdateTime(now);
					builder.setExpirationTime(oldMessage.expirationTime);
					builder.setCancellation(true);
					outMap.put(oldMessage.id, builder.build());
				}
		List<TraffMessage> res = new ArrayList<TraffMessage>();
		res.addAll(outMap.values());
		return res;
	}
}
