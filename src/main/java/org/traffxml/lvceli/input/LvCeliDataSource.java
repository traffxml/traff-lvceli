/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-lvceli library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.lvceli.input;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.lvceli.LvCeliFeed;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.input.DataSource;

public class LvCeliDataSource extends DataSource {
	/**
	 * @brief The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	/* Do not use the MethodHandles hack here, in order to support Android */
	static final Logger LOG = LoggerFactory.getLogger(LvCeliDataSource.class);

	private Date lastUpdate = null;

	public LvCeliDataSource(String id, String url, Properties properties) {
		super(id, url, properties);
	}

	@Override
	public Date getLastUpdate() {
		return lastUpdate;
	}

	@Override
	public int getMinUpdateInterval() {
		return 0;
	}

	@Override
	public boolean needsExistingMessages() {
		return true;
	}

	@Override
	public Collection<TraffMessage> poll(Collection<TraffMessage> oldMessages, int pollInterval) {
		URL url = getUrl();
		LvCeliFeed feed = null;
		try {
			InputStream dataInputStream;
			URLConnection connection = url.openConnection();
			/* if content is gzipped, unzip it on the fly */
			if ("application/gzip".equals(connection.getContentType())
					|| "application/x-gzip".equals(connection.getContentType())
					|| "gzip".equals(connection.getContentEncoding()))
				dataInputStream = new GZIPInputStream(connection.getInputStream());
			else
				dataInputStream = connection.getInputStream();
			feed = LvCeliFeed.parseJson(dataInputStream);
			dataInputStream.close();
			return feed.toTraff(id, oldMessages); // TODO pollInterval
		} catch (MalformedURLException e) {
			LOG.debug("{}", e);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
		return null;
	}
}
